package service

import "fmt"

type Shifter struct {
	task []Data
	out  [][]int
}

type Data struct {
	array []int
	cycle int
	err   string
}

func (s Shifter) Shifter(task []Data) Shifter {
	fmt.Println("Make a variables")
	s.task = task
	s.out = make([][]int, len(s.task))
	for i := 0; i < len(task); i++ {
		s.out[i] = (s).Solution(s.task[i].array, s.task[i].cycle)
	}

	return s
}

func (s Shifter) Solution(array []int, cycle int) []int {

	newArray := make([]int, len(array))

	leng := len(array)

	var trueCycle int

	if cycle >= leng {
		trueCycle = cycle - (cycle/leng)*leng
	} else {
		trueCycle = cycle
	}

	if trueCycle != 0 {

		for i := 0; i < trueCycle; i++ {
			newArray[((trueCycle - 1) - i)] = array[(leng-1)-i]

			if i == (trueCycle - 1) {
				for j := i; j < leng-1; j++ {

					newArray[j+1] = array[j-i]

				}
			}
		}
		return newArray
	}
	return array
}

func (s Data) Read(task []byte) (n int, err error) {

	n = (len(s.array) + 1)
	err = s
	return n, s

}

func (s Data) Error() string {
	if len(s.array) == 0 {
		s.err = "Пустой массив"
		return s.err
	}
	return ""
}
