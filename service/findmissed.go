package service

import (
	"fmt"
)

type FindeMissed struct {
	result int
}

func (f FindeMissed) Solution(A []int) int {

	var res int
	if len(A) == 0 {
		panic("Пустой массив, я так не работаю!")
	}
	for i := 0; i < len(A); i++ {
		j := i
		for ; j > 0 && A[i] < A[j-1]; j-- {
		}
		for ; i > j; i-- {
			A[i], A[i-1] = A[i-1], A[i]
		}
	}
	fmt.Println(A)

	if A[0] <= 0 {
		panic("У вас числа отрицательные или ноль, я так не работаю!")
	}

	for i := 0; i < len(A); i++ {
		if A[i] != i+1 {
			res = i + 1
			break
		}
	}

	if res == 0 {
		panic("Нет пропущенного элемента")
	}
	return res
}
