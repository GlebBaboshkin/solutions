package service

import (
	"fmt"
	"io"
	"math"
	"net/http"
	"strconv"
)

type ReadData struct {
	shifter  Shifter
	searcher FindLonely
	missed   FindeMissed
	sequence SequenceChek
}

func CreateNewShifter() Shifter {
	var res Shifter
	var data, data1, data2, data3, data4, data5, data6 Data
	data.array = []int{3, 4, 6, 8, 11, 15, 25}
	data.cycle = 3

	data1.array = []int{6, 8, 11, 15, 25}
	data1.cycle = 8

	data2.array = []int{9, 15, 22, 30, 35, 45, 78, 99, 51}
	data2.cycle = 5

	data3.array = []int{31, 40, 12, 33, 45}
	data3.cycle = 5

	data4.array = []int{31, 40, 12, 33, 45}
	data4.cycle = 1

	data5.array = []int{31, 40, 12, 33, 45}
	data5.cycle = 0

	data6.array = []int{31, 40, 12, 33, 45}
	data6.cycle = 11

	creationData := []Data{data, data1, data2, data3, data4, data5, data6}
	fmt.Println("Creating obj...")
	res = (res).Shifter(creationData)
	fmt.Println("Created obj...")
	return res
}

func ParseStringArray(nhw []string) ([]int, int) {
	var num, Num int
	var B []int
	var separator int
	for i := 0; i < len(nhw)-2; i++ {

		if nhw[i] == "%" && nhw[i+1] == "5" && nhw[i+2] == "B" {
			i += 2
			fmt.Print("1")
		} else if nhw[i] == "%" && nhw[i+1] == "2" && nhw[i+2] == "C" {
			i += 2
			fmt.Print("abort")
		} else if nhw[i] == "%" && nhw[i+1] == "5" && nhw[i+2] == "D" {
			separator = len(B) - 1
			i += 2
		} else {
			for j := i; j < len(nhw) && nhw[j+1] != "%" && nhw[j+1] != "="; j++ {
				num = j - i + 1
				fmt.Print("2")
			}
			for k := 0; k <= num; k++ {
				A, _ := strconv.Atoi(nhw[i+num-k])
				Num += A * int(math.Pow(10, float64(k)))
				fmt.Print("3")
			}
			B = append(B[:], Num)
			i += num
			Num = 0
			num = 0
			fmt.Print("add")
		}

	}

	//fmt.Println(A)
	if separator == len(B) {
		separator = 0
	}
	fmt.Println(B)
	fmt.Print(B[:separator], B[separator:])
	fmt.Println(nhw)
	return B, separator
}

func ParseByteArray(hw []byte) ([]int, int) {

	var num, Num, separator, A int
	var task []int

	for i := 0; i < len(hw)-1; i++ {
		if hw[i] == 91 {

		} else if hw[i] == 93 {

		} else if hw[i] == 44 {

		} else {
			for j := i; j < len(hw)-1 && hw[j+1] != 44 && hw[j+1] != 93; j++ {
				num = j - i + 1
				fmt.Print("2")
			}
			for k := 0; k <= num; k++ {
				A = int(hw[i+num-k]) - 48
				Num += A * int(math.Pow(10, float64(k)))
				fmt.Print("3")
			}
			task = append(task, Num)
			if hw[i+1] == 93 {
				separator = len(task) - 1
			}
			i += num
			Num = 0
			num = 0
		}
	}
	return task, separator
}

func (s *ReadData) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	fmt.Println("In Serve...")
	if path == "/tasks/Циклическая_ротация" {

		fmt.Println("In Path...")
		//hw := r.URL.Query().Encode()
		hw, err := io.ReadAll(r.Body)

		fmt.Printf("r.Body: %v\n", hw)

		arr, sep := ParseByteArray(hw)

		fmt.Println(arr[:sep], arr[sep], sep)
		w.WriteHeader(http.StatusOK)
		_, err = w.Write([]byte(hw))
		var task Data

		task.array = append(task.array, arr[:sep]...)
		task.cycle = arr[sep]

		s.shifter.task = append(s.shifter.task, task)
		sol := s.shifter.Solution(task.array, task.cycle)
		s.shifter.out = append(s.shifter.out, sol)
		fmt.Println(s.shifter)
		if err != nil {

		}

	}
	fmt.Printf("конец")
	//return
}

func MakeHandler() error {
	fmt.Println("MakeHandler...")
	return http.ListenAndServe("localhost:8181", &ReadData{})
}
